#!/bin/python3

import csv
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

model = LinearRegression()

with open('bench.csv') as file:
    data = csv.reader(file, delimiter=',')

    x = []
    y = []
    first_row = True
    for row in data:
        # Ignore first row (headers row)
        if first_row:
            first_row = False
            continue
        else:
            x.append(float(row[0]) * float(row[0]))
            # Convert to milliseconds
            y.append(float(row[1]) * 1e3)

    x_new = np.array(x).reshape(-1, 1)
    model.fit(x_new, y)
    y_new = model.predict(x_new)

    plt.xlabel('Number of pixels in the image')
    plt.ylabel('Mean computation time (ms)')

    plt.scatter(x, y, color="orange", label="Actual data")
    plt.plot(x_new, y_new, label="Linear regression")
    plt.legend()
    plt.show()
