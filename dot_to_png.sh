#!/bin/sh

# Quick little bash script to execute the necessary commands to turn a dot file
# into an image file
# Input: the dot file's name **WITHOUT** the extension

dot $1.dot -Tpng > $1.png