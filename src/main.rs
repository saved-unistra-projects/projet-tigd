extern crate env_logger;
extern crate log;

pub mod args;
use algorithms::interpolation::{Interpolate};
use args::read_argument;

extern crate image;
use image::io::Reader as ImageReader;

mod types;
mod utils;
use crate::types::pixel::Pixel;
use crate::types::tree::Node;

mod algorithms;
use crate::algorithms::canonicalize_tree::canonicalize_tree;
use crate::algorithms::sort;
use crate::algorithms::interpolation::ImageInterpolate;
use crate::algorithms::union_find::union_find;

fn main() {
    // Parse command line
    let args = read_argument();
    // Initialize logger
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .parse_default_env()
        .init();

    // Open image
    let input_path = args.value_of("input").unwrap();
    let mut img = ImageReader::open(input_path)
        .expect("An error occured while opening the input image file")
        .decode()
        .expect("An error occured while decoding the image")
        .to_luma8();


    let mut un_interpolate : bool = false;
    let string_method = args.value_of("method").unwrap();
    let sorted_vector = match string_method {
        "max" => sort::MaxTreeSorter::sort(&img),
        "shapes" => {
            let image_interpolate = ImageInterpolate::interpolate(&img);
            image_interpolate.save("out_interpolate.png")
                .expect("Failed to write the image interpolate");
            let return_sort = sort::InterpolationSorter::sort(&image_interpolate);
            img = return_sort.1;
            un_interpolate = true;
            return_sort.0
        },
        _ => {
            log::error!("This sorting method is currently not implemented");
            panic!();
        }
    };

    // Compute the morphological tree
    let mut tree = union_find(sorted_vector, img.dimensions());
    // Canonicalize the tree
    log::info!(
        "Start tree canonicalization, current nodes: {}",
        tree.arena
            .clone()
            .into_iter()
            .filter(|x: &Node<_>| x.parent.is_some())
            .collect::<Vec<Node<Pixel>>>()
            .len()
    );
    canonicalize_tree(&mut tree);
    log::info!(
        "Tree canonicalization ended, remaining nodes: {}",
        tree.arena
            .clone()
            .into_iter()
            .filter(|x: &Node<_>| x.parent.is_some())
            .collect::<Vec<Node<Pixel>>>()
            .len()
    );


    if un_interpolate {
        log::info!("Start un-interpolate...");
        img = ImageInterpolate::un_interpolate(&img, &mut tree);
        log::info!("...Ending un-interpolate !");
    }

    
    //    let color_vec = tree.choose_colors

    log::info!("Printing tree to output file");
    let output_path = args.value_of("output_path").unwrap();

    let colors = tree.choose_colors(args.occurrences_of("use_original_colors") > 0);
    tree.print_to_dot(output_path, &colors);

    // Create the reference image
    let ref_image = utils::reference_image(&img, &colors, &tree);
    let ref_output_path = args.value_of("output_reference_path").unwrap();
    ref_image
        .save(ref_output_path)
        .expect("Failed to write the reference file");
}
