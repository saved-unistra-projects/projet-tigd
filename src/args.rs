//! Handle command line arguments

extern crate clap;
use clap::{App, Arg};

pub fn read_argument() -> clap::ArgMatches<'static> {
    let args = App::new(clap::crate_name!())
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .arg(
            Arg::with_name("input")
                .short("i")
                .long("input")
                .help("Sets the input file to use")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("method")
                .short("m")
                .long("method")
                .help("Chose the method to use (R sorting method)")
                .required(true)
                .takes_value(true)
                .possible_values(&["max", "shapes"])
                .default_value("max"),
        )
        .arg(
            Arg::with_name("output_path")
                .short("o")
                .long("output")
                .help("Set the output file where the resulting tree will be written in dot format")
                .required(true)
                .takes_value(true)
                .default_value("out.dot"),
        )
        .arg(
            Arg::with_name("output_reference_path")
                .short("r")
                .long("reference")
                .help("Chose to output file for the image reference")
                .required(true)
                .takes_value(true)
                .default_value("out_ref.png"),
        )
        .arg(
            Arg::with_name("use_original_colors")
                .long("use-original-colors")
                .help(
                    "When printing the tree to dot format, use the original
                colors instead of procedurally generated ones",
                )
                .takes_value(false)
                .multiple(false),
        )
        .get_matches();
    return args;
}
