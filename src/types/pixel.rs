//! Pixel structure and related implementations.
use crate::types::neighbour::Connexity;
use std::fmt::Display;

/// Simple pixel structure.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Pixel {
    /// X coordinate of the pixel
    pub x: u32,
    /// Y coordinate of the pixel
    pub y: u32,
    /// Color value of the pixel
    pub value: u8,
}

impl Pixel {
    /// Create a new pixel.
    /// ### Arguments
    /// * `x` - X coordinate
    /// * `y` - y coordinate
    /// * `value` - color value (grayscale)
    /// ### Return value
    /// The constructed pixel object
    pub fn new(x: u32, y: u32, value: u8) -> Self {
        Self { x, y, value }
    }
}

impl Connexity for Pixel {
    /// We will consider 4-connexity here.
    /// This is simply an implementation of the `neighbours`
    /// function in the `Connexity` trait.
    fn neighbours(&self) -> Vec<Self>
    where
        Self: Sized,
    {
        let mut neighbours = vec![
            Pixel::new(self.x + 1, self.y, 0),
            Pixel::new(self.x, self.y + 1, 0),
        ];
        // Check for inferior boundaries since we consider unsigned coordinates
        if self.x != 0 {
            neighbours.push(Pixel::new(self.x - 1, self.y, 0));
        }
        if self.y != 0 {
            neighbours.push(Pixel::new(self.x, self.y - 1, 0));
        }
        neighbours
    }
}

impl Display for Pixel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "x: {}, y: {}, value: {}", self.x, self.y, self.value)
    }
}
