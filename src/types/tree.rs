//! Tree data structure and related types and functions

use crate::types::pixel::Pixel;
use std::collections::HashMap;
use std::fs::File;
use std::io::Write;

/// Simple type definition for color maps in case the encoding changes
pub type ColorMap = HashMap<usize, (f32, f32, f32)>;

/// Tree structure.
/// This is a simple tree structure using an arena
/// (<https://rust-leipzig.github.io/architecture/2016/12/20/idiomatic-trees-in-rust/>).
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Tree<T> {
    /// The arena (made public for simplicity)
    pub arena: Vec<Node<T>>,
    /// The root of the tree
    pub root: Option<usize>,
}

impl<T: Clone> Tree<T> {
    /// Get the node defined by the given index
    /// ### Arguments
    /// * `index` - The index of the node to get
    /// ### Return value
    /// The corresponding node
    /// ### Note
    /// The node is copied, so no modification to the node
    /// will affect the tree structure.
    pub fn get(&self, index: usize) -> Node<T> {
        self.arena[index].clone()
    }


    /// Get the node defined by the given index
    /// ### Arguments
    /// * `index` - The index of the node to get
    /// ### Return value
    /// The corresponding node in mutable
    pub fn get_mut(&mut self, index: usize) -> &mut Node<T> {
        self.arena.get_mut(index).unwrap()
    }
} 

impl<T: Clone + std::fmt::Debug> Tree<T> {
    /// Create a new tree.
    /// ### Return value
    /// The created tree
    pub fn new() -> Self {
        Self {
            arena: Vec::new(),
            root: None,
        }
    }

    /// Set (or unset) the parent of a given node
    /// ### Arguments
    /// * `index` - The index of the node for which to set the parent
    /// * `index_parent` - The index of the new parent (or `None`)
    /// ### Note
    /// This both changes the parent, and updates the old parent and new parents
    /// child relationships.
    pub fn set_parent(&mut self, index: usize, index_parent: Option<usize>) {
        
        let old_parent = self.arena[index].parent;
        // Remove the child in the old parent (if there was one)
        if old_parent.is_some() {
            self.arena[old_parent.unwrap()].remove_child(index);
        }
        self.arena[index].set_parent(index_parent);
        // Add the child in the new parent (if there is one)
        if index_parent.is_some() {
            self.arena[index_parent.unwrap()].add_child(index);
        }
        log::trace!(
            "Set parent of node {} from {:?} to {:?}",
            index,
            old_parent,
            index_parent
        );
    }

    /// Set (or unset) the root of the tree
    /// ### Arguments
    /// * `root` - Index of the node that will become the new root (or `None`)
    pub fn set_root(&mut self, root: Option<usize>) {
        log::trace!("Set root of tree to {:?}", root);
        self.root = root;
    }

    /// Add a node to the arena
    /// ### Arguments
    /// * `node` - The node to add to the tree
    /// ### Return value
    /// The index of the added node in the arena
    pub fn add_node(&mut self, node: Node<T>) -> usize {
        self.arena.push(node);
        log::trace!(
            "Added node {:?} to the tree with index {}",
            self.arena.last(),
            self.arena.len() - 1
        );
        return self.arena.len() - 1;
    }
}

impl Tree<Pixel> {
    /// Print the tree in dot format.
    /// The names used for each node are their indices in the arena.
    /// ### Arguments
    /// * `file` - The file in which the tree will be written
    /// * `colors` - A hash map that associates each node with a parent to a hue
    pub fn print_to_dot(&self, file: &str, colors: &ColorMap) {
        if self.root.is_none() {
            eprintln!("No root !");
            return;
        }
        // Open the file
        log::info!("Creating file \"{}\"", file);
        let mut file = File::create(file).expect("Failed to create file");
        writeln!(&mut file, "graph {{").expect("Failed to write to file");
        self.print_to_dot_aux(self.root.unwrap(), &mut file, colors);
        writeln!(file, "}}").expect("Failed to write to file");
        // Ne need to close the file because this is Rust baby
    }

    /// Auxiliary (recursive) function for the print_to_dot function.
    fn print_to_dot_aux(&self, root: usize, file: &mut File, colors: &ColorMap) {
        let children = self.get(root).children;
        let color = colors.get(&root).unwrap();
        writeln!(
            file,
            "{} [style=filled] [fillcolor=\"{},{},{}\"] [label=\"\"]",
            root, color.0, color.1, color.2
        )
        .expect("Failed to write to file");
        for child in children {
            if child != root {
                writeln!(file, "{} -- {}", root, child).expect("Failed to write to file");
                self.print_to_dot_aux(child, file, colors);
            }
        }
    }

    /// Choose a color for each node that has a parent.
    /// ### Arguments
    /// * `use_original` - Use the original pixel's colors instead of generating new ones
    /// ### Return value
    /// A hash map that associates each node index to a hue value.
    pub fn choose_colors(&self, use_original: bool) -> ColorMap {
        log::info!("Choosing colors for the nodes");
        let mut nodes_vec = Vec::new();
        let mut hash = ColorMap::new();
        for (i, node) in self.arena.iter().enumerate() {
            if node.parent.is_some() {
                nodes_vec.push(i);
            }
        }
        // For each node divide its index in the vector by the maximum number of nodes
        let nodes_len = nodes_vec.len() as f32;
        for (i, index) in nodes_vec.iter().enumerate() {
            if use_original {
                hash.insert(
                    *index,
                    (0., 0., (self.get(*index).element.value as f32 / 255.)),
                );
            } else {
                hash.insert(*index, (i as f32 / nodes_len, 1., 1.));
            }
            log::trace!(
                "Chose a color of {:?} for the node with index {}",
                hash.get(index).unwrap(),
                index
            );
        }
        log::info!("Color choice ended");
        hash
    }
}

/// Generice node structure.
/// Every relationship is coded using `usize` because the nodes are
/// made to work in an arena-like structure (see `Tree`)
#[derive(Clone, Debug, PartialEq)]
pub struct Node<T> {
    /// Data held by the node
    pub element: T,
    /// Parent of the node
    pub parent: Option<usize>,
    /// Children of the node
    pub children: Vec<usize>,
}

impl<T> Node<T> {
    /// Create a new node
    /// ### Arguments
    /// * `element` - The data to be held by the node
    /// ### Return value
    /// The created node
    pub fn new(element: T) -> Self {
        Self {
            element,
            parent: None,
            children: Vec::new(),
        }
    }

    /// Set the parent of the node
    /// ### Arguments
    /// * `target` - The new parent (or `None`)
    pub fn set_parent(&mut self, target: Option<usize>) {
        self.parent = target;
    }

    /// Add the given child to the children list
    /// ### Arguments
    /// * `target` - The target child
    pub fn add_child(&mut self, target: usize) {
        self.children.push(target);
    }

    /// Remove the given child from the children list, leveraging the `Vec::retain` function.
    /// ### Arguments
    /// * `target` - Index of the child to remove
    pub fn remove_child(&mut self, target: usize) {
        self.children.retain(|&x| x != target);
    }
}
