//! The types module regroups all data structures and type names used in the project
pub mod neighbour;
pub mod pixel;
pub mod tree;
