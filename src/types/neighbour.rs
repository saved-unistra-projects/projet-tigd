//! This module defines traits for connexity

/// Connexity relationship between objects
pub trait Connexity {
    /// Returns the neighbours of a given object
    /// ### Arguments
    /// None
    /// ### Return value
    /// A vector of objects of the same type as the initial object, representing its neighbours
    fn neighbours(&self) -> Vec<Self>
    where
        Self: Sized;
}
