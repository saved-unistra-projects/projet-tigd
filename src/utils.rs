//! Utility functions

extern crate image;
use image::{GrayImage, Rgba, RgbaImage};
extern crate angular_units;
extern crate prisma;
use crate::types::tree::ColorMap;
use angular_units::Deg;
use prisma::FromColor;
use prisma::{Hsv, Rgb};

use crate::types::pixel::Pixel;
use crate::types::tree::Tree;

/// Create a reference image.
/// ### Arguments
/// * `img` - The input image
/// * `colors` - A hash map associating each pixel coordinate to a color (hue)
/// * `tree` - The morphologic tree
/// ### Return value
/// An Rgba image that serves as reference (each canonical pixel is colored appropriately)
pub fn reference_image(img: &GrayImage, colors: &ColorMap, tree: &Tree<Pixel>) -> RgbaImage {
    log::info!("Creating reference image");
    // Build a palette for the image palette extension
    let mut palette = [(0, 0, 0); 256];
    for i in 0..256 {
        let val = i as u8;
        palette[i] = (val, val, val);
    }
    let mut new_image = img.clone().expand_palette(&palette, None);
    for index in colors.keys() {
        let pixel = tree.get(*index).element;
        let color = colors[index];
        // Convert (cumbersomely...) the hsv value to rgb
        let rgb = Rgb::from_color(&Hsv::new(Deg(color.0 * 360.), color.1, color.2));
        log::trace!(
            "Coloring pixel ({}, {}) with color {:?}",
            pixel.x,
            pixel.y,
            rgb
        );
        new_image.put_pixel(
            pixel.x,
            pixel.y,
            Rgba::<u8>([
                (rgb.red() * 255.) as u8,
                (rgb.green() * 255.) as u8,
                (rgb.blue() * 255.) as u8,
                255,
            ]),
        )
    }
    log::info!("Reference image creation ended");
    new_image
}
