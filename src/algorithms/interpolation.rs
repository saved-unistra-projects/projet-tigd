extern crate image;
extern crate log;

use image::{GrayImage, RgbImage, Rgb};
use std::cmp;
use crate::types::pixel::Pixel;
use crate::types::tree::Tree;

// Image Interpolated
pub type U = RgbImage;

pub trait Interpolate {
    /**
     * Interpolate a gray image : so the set-valued map
     * R : lower interval  
     * G : value
     * B : upper interval 
     * 
     * if no interval for a pixel : R = B = G  
     */
    fn interpolate(_ : &GrayImage) -> U;
    
    /**
     * Un_interpolate a gray image : so the inverse of the function interpolate
     */
    fn un_interpolate(_ : &GrayImage, _ : &mut Tree<Pixel>) -> GrayImage;
}

pub struct ImageInterpolate;

impl Interpolate for ImageInterpolate {
    fn interpolate(img : &GrayImage) -> U {
        log::info!("Starting Interpolation...");

        // Create new image
        let new_width = (img.dimensions().0 * 4) - 1;
        let new_height = (img.dimensions().1 * 4) - 1;
        let mut img_interpolate = U::new(new_width, new_height);

        // Pixels of the original image
        // And
        // Pixels (1/2(H_1)^n)\D
        for y in (0..new_height-1).step_by(2) {
            for x in (0..new_width-1).step_by(2) {
                let mut val;
                // Pixels of the original image
                if x % 4 == 0 && y % 4 == 0 {
                    val = img
                        .get_pixel(x/4, y/4)
                        .0
                        .first()
                        .expect("ERROR : fonction intropale", )
                        .to_owned();
                }
                // Pixels (1/2(H_1)^n)\D
                else {
                    val = u8::MIN;

                    let x2 : f32 = (x as f32)/4.0;
                    let y2 : f32 = (y as f32)/4.0;
                    for _x  in -1..=1 {
                        for _y in -1..=1{
                            let new_x = x2 + (_x as f32) / 2.0;
                            let new_y = y2 + (_y as f32) / 2.0;

                            if  new_x >= 0.0 && 
                                new_y >= 0.0 && 
                                new_x < (img.dimensions().0 as f32) && 
                                new_y < (img.dimensions().1 as f32){

                                if  new_x % 1.0 == 0.0 &&
                                    new_y % 1.0 == 0.0  {

                                    val = cmp::max(val, 
                                    img
                                        .get_pixel(new_x as u32, new_y as u32)
                                        .0
                                        .first()
                                        .expect("ERROR : fonction intropale", )
                                        .to_owned());
                                }
                            }
                        }
                    }
                }             
                
                img_interpolate.put_pixel(x + 1, y + 1, Rgb([val, val, val]));
            }
        }

        // Pixels X\(1/2(H_1)^n)
        for x in 0..new_width {
            for y in 0..new_height {
                if x % 2 ==  0 || y % 2 == 0 {
                    let mut min = u8::MAX;
                    let mut max = u8::MIN;
    
                    for _x  in -1..=1 {
                        for _y in -1..=1 {
                            let new_x = (x as i32) + _x;
                            let new_y = (y as i32) + _y;
                            if  new_x >= 0 && 
                                new_y >= 0 {
                                let x2 : u32 = new_x as u32;
                                let y2 : u32 = new_y as u32;
    
                                if  x2 % 2 == 1 &&
                                    y2 % 2 == 1 && 
                                    x2 < new_width && 
                                    y2 < new_height {
                                    let val = img_interpolate
                                        .get_pixel(x2, y2)
                                        .0
                                        .first()
                                        .expect("ERROR : fonction intropale", )
                                        .to_owned();
                                    max = cmp::max(max, val);
                                    min = cmp::min(min, val);
                                }
                            }
                        }
                    }
    
                    img_interpolate.put_pixel(x, y , Rgb([min,(( min as u32  + max as u32 ) / 2) as u8, max]));
                }
            }
        }

        log::info!("...Interpolation Finished !");

        return img_interpolate;
    }

    fn un_interpolate(img : &GrayImage, tree : &mut Tree<Pixel>) -> GrayImage {
        let mut img_un_interpolate = GrayImage::new((img.width() + 1) / 4, (img.height() + 1) / 4);

        for x in 0..img_un_interpolate.width() {
            for y in 0..img_un_interpolate.height() {
                img_un_interpolate.put_pixel(x, y, *img.get_pixel( (x * 4) + 1, (y * 4) + 1));
            }
        }


        let mut next_node = tree.get(tree.root.unwrap()).children;
        let mut i = 0;
        while i != next_node.len() {
            let node = next_node[i];
            
            // Nodes on intervals are refocused in the good coordinate
            let pixel = Pixel::new(tree.get_mut(node).element.x.clone(), 
                tree.get_mut(node).element.y.clone(), 
                img.get_pixel(tree.get_mut(node).element.x.clone(), tree.get_mut(node).element.y.clone()).0[0]);
            if  pixel.x % 4 == 0 ||
                pixel.x % 4 == 3  {                
                if pixel.x >0 && img.get_pixel(pixel.x - 1, pixel.y).0[0] != pixel.value {
                    tree.get_mut(node).element.x = tree.get_mut(node).element.x + 1;
                }
                else if pixel.x < img.width() - 1 && img.get_pixel(pixel.x + 1, pixel.y).0[0] != pixel.value {
                    tree.get_mut(node).element.x = tree.get_mut(node).element.x - 1;
                }

            }
            if  pixel.y % 4 == 0 ||
                pixel.y % 4 == 3 {

                if pixel.y > 0 && img.get_pixel(pixel.x , pixel.y - 1).0[0] != pixel.value {
                    tree.get_mut(node).element.y = tree.get_mut(node).element.y + 1;
                }
                else if pixel.y < img.height() - 1 && img.get_pixel(pixel.x, pixel.y + 1).0[0] != pixel.value {
                    tree.get_mut(node).element.y = tree.get_mut(node).element.y - 1;
                }

            }

            // Divise coordinate of tree
            tree.get_mut(node).element.x = (tree.get_mut(node).element.x) / 4;
            tree.get_mut(node).element.y = (tree.get_mut(node).element.y) / 4;

            for child in tree.get(node).children {
                if child != node &&
                    !next_node.contains(&child){
                    next_node.push(child);
                }
            }

            i += 1;
        }


        return img_un_interpolate;
    }
}