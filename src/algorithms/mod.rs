//! The algorithm module regroups all algorithm related structures and functions

pub mod canonicalize_tree;
pub mod sort;
pub mod union_find;
pub mod interpolation;
