//! Functions and helper functions for the union find algorithm. Used to construct
//! the morphological tree associated with the image and the sorted input vector.

extern crate log;

use std::collections::HashMap;

use crate::types::neighbour::Connexity;
use crate::types::pixel::Pixel;
use crate::types::tree::{Node, Tree};

/// Ordered list of pixesl.
type R = Vec<Pixel>;

/// The morphological tree (represents the `parent` relationship between nodes).
type T = Tree<Pixel>;

/// A hash map used to save a different parent relationship from the tree. Used
/// by the `find_root` function.
type Zpar = HashMap<usize, Option<usize>>;

/// A hash map used to associate the coordinates of a pixel within the image to its node id.
type Nodes = HashMap<(u32, u32), usize>;

/// Union find function used to compute the morphological tree
/// ### Arguments
/// * `r` - A sorted vector of pixels. The sorting algorithm used will determine
///     what kind of tree is computed implicitly
/// * `size` - Size of the image from which we want to extract the tree. This is
///     used for boundary checks.
/// ### Return value
/// The morphological tree computed from the image and sorted vector.
pub fn union_find(r: R, size: (u32, u32)) -> T {
    log::info!("Starting union_find");

    // Declare our different structures
    let mut zpar = Zpar::new();
    let mut nodes = Nodes::new();
    let mut tree = Tree::new();

    /* Create a node for each pixel and add it to the tree (relationships aren't
       computed at this point).
    */
    for i in 0..r.len() {
        // Create and add the node
        let node = Node::new(r[i]);
        let index = tree.add_node(node);
        /* Fill our nodes hashmap as well to keep track of the relationship between
           coordinates and tree indices
        */
        nodes.insert((r[i].x, r[i].y), index);
        // Initialize everything in the zpar hashmap to `None`
        zpar.insert(index, None);
    }

    // Run through the pixel list in reverse
    for i in (0..nodes.len()).rev() {
        log::trace!("Treating node {}", i);
        // Let p be the current pixel / node
        let p = i;

        // Shortcut to acces the pixel coordinates and value
        let elem = tree.get(p).element;

        // For each pixel we start by making them their own parents and filling zpar
        tree.set_parent(p, Some(p));
        zpar.insert(p, Some(p));

        // Run through each neighbour, leveraging the `neighbours` function from
        // the `Connexity` trait
        log::trace!("Neighbours of {} ({:?}): {:?}", p, elem, elem.neighbours());
        for n in elem.neighbours() {
            log::trace!("Treating neighbour of {} : {}", p, n);
            // Boundary check
            if !is_in_borders(n.x, n.y, size) {
                log::trace!("Pixel is out of bounds, continuing...");
                continue;
            }
            let n_index = nodes.get(&(n.x, n.y)).unwrap().to_owned();
            log::trace!("Found pixel {:?} to correspond to node {}", n, n_index);
            // Skip every neighbour with an uninitialized zpar
            if zpar.get(&n_index).unwrap().is_none() {
                log::trace!("Zpar is undefined, continuing...");
                continue;
            }
            // Find the root of the neighbour
            let root = find_root(&mut zpar, n_index);

            if root != p {
                // Change the parent of our root to keep building our tree
                log::trace!("Changing parent of {} to {}", root, p);
                tree.set_parent(root, Some(p));
                zpar.insert(root, Some(p));
            }
        }
    }
    /* We are now going to start from our last treated node and
    go up the parent relationship to the root of the tree */
    let mut node_index = nodes.len() - 1;
    while tree.get(node_index).parent.is_some()
        && tree.get(node_index).parent.unwrap() != node_index
    {
        let old_node_index = node_index;
        node_index = tree.get(node_index).parent.unwrap();
        log::trace!(
            "Going up the tree from {} to {}",
            old_node_index,
            node_index
        );
    }
    // Set the root
    log::info!("Setting the root of the tree to {}", node_index);
    tree.set_root(Some(node_index));
    log::info!("Union find ended successfully");
    return tree;
}

/// Find the current root of a given node (pixel)
/// ### Arguments
/// * `zpar` - The zpar relationship used to compute and root, and which will
///     modified to update said root
/// * `x` - The index of the node if which we want to find the root
/// ### Return value
/// The index of the discovered root
fn find_root(zpar: &mut Zpar, x: usize) -> usize {
    log::trace!("Started find root for {}", x);
    let ret;
    let xzpar = zpar.get(&x).unwrap().unwrap();
    if xzpar == x {
        ret = x;
    } else {
        let x_prim = find_root(zpar, xzpar);
        zpar.insert(x, Some(x_prim));
        ret = x_prim;
    }
    log::trace!("Found {} as the root for {}", ret, x);
    ret
}

/// Simple function to check if the given coordinates are within a given boundary
/// ### Arguments
/// * `x` - X coordinate of the pixel
/// * `y` - Y coordinate of the pixel
/// * `size` - Size of the image
/// ### Return value
/// * `true` if the pixel if within boundaries
/// * `false` otherwise
/// ### Note
/// The function only checks upper boundaries since the coordinates are
/// unsigned.
fn is_in_borders(x: u32, y: u32, size: (u32, u32)) -> bool {
    return x < size.0 && y < size.1;
}
