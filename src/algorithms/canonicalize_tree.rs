//! This module holds the tree canonicalization algorithm.

extern crate log;

use crate::types::pixel::Pixel;
use crate::types::tree::Tree;

/// Transform the given tree to give it its canonical form.
///
/// The function will remove unecessary nodes, they are node which have the same
/// color value as their parent. In fact, we only keep the nodes which represent a
/// shape in the image, these nodes represent the "canonical" pixels of a shapes,
/// or the representative pixel of this shape. The function is necessary in order to
/// obtain a readable tree structure.
///
/// ---
///
/// Excerpt from the paper:
/// > "Obtaining the following extra
/// > property, “5. ∀p, parent(p) is a canonical element,” is extremely interesting
/// > since it ensures that the parent function, when restricted to canonical elements
/// > only, gives a “compact” morphological tree [...] Precisely it allows to
/// > browse components while discarding their contents:
/// > a traversal is thus limited to one element (one pixel) per component, instead of
/// > passing through every image elements (pixels)."
/// ### Arguments
/// * `tree` - The tree to modify (simplfy)
/// ### Return value
/// No return value but the input tree will be modified (keep in mind that every
/// node will still be present, some just won't have any parent)

pub fn canonicalize_tree(tree: &mut Tree<Pixel>) {
    let mut childrens = tree.get(tree.root.unwrap()).children;
    let mut attachs = Vec::new();
    for _ in 0..childrens.len() {
        attachs.push(tree.root.unwrap());
    }
    childrens.reserve(tree.arena.len());
    attachs.reserve(tree.arena.len());

    let mut i = 0;
    while i != childrens.len() {
        let node = childrens[i];
        let attach = attachs[i];

        // Avoid looping indefinitely...
        for child in tree.get(node).children {
            if child != node {
                if tree.get(child).element.value != tree.get(attach).element.value {
                    // In this case, the value is different: this is a canonical node !
                    log::trace!(
                        "Changed parent of {} from {} to {}",
                        child,
                        tree.get(child).parent.unwrap(),
                        attach
                    );
                    // Change the parent of the node
                    tree.set_parent(child, Some(attach));

                    // Call recursively with a new attach point
                    childrens.push(child);
                    attachs.push(child);
                } else {
                    // We don't want this node

                    // Set it aside
                    tree.set_parent(child, None);

                    // Call recursively with the same attach point
                    childrens.push(child);
                    attachs.push(attach);
                }
            }
        }
        i += 1;
    }
}

/* Raphi version
pub fn canonicalize_tree(tree: &mut Tree<Pixel>) {
    // Excplicit stack of nodes
    // Represents the node to be treated and the one to attach to
    let mut node_stack = Vec::new();
    // Node that is currently treated
    let node = tree.root.unwrap();
    node_stack.push((node, node));

    while !node_stack.is_empty() {
        let (node, attach) = node_stack.pop().unwrap();
        let children = tree.get(node).children;
        for child in children {
            // Avoid looping indefinitely...
            if child != node {
                if tree.get(child).element.value != tree.get(attach).element.value {
                    // In this case, the value is different: this is a canonical node !
                    log::trace!(
                        "Changed parent of {} from {} to {}",
                        child,
                        tree.get(child).parent.unwrap(),
                        attach
                    );
                    // Change the parent of the node
                    tree.set_parent(child, Some(attach));
                    // Add the nodes to the stack accordingly
                    node_stack.push((child, child));
                } else {
                    // We don't want this node

                    // Set it aside
                    tree.set_parent(child, None);
                    // Call recursively with the same attach point
                    node_stack.push((child, attach));
                }
            }
        }
    }
}*/
