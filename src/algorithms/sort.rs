//! Sorting module that regroups all sorting structures.
//! Mainly for the max-tree and tree-of-shape computations.

extern crate image;
extern crate log;

use std::{ collections::HashMap};

use crate::types::{pixel::Pixel, neighbour::Connexity};
use image::{GrayImage, RgbImage, Luma, Rgb};

/// Sorter for the max tree computation.
/// Simply sorts the pixels of the image (gray image) from
/// brightest to darkest
pub struct MaxTreeSorter;

impl MaxTreeSorter {
    /// Sorting function
    /// ### Arguments
    /// * `img` - The image to be treated
    /// ### Return value
    /// Returns a sorted array of Pixels
    pub fn sort(img: &GrayImage) -> Vec<Pixel> {
        log::info!("Starting MaxTree sort");
        // Initialize empty vector
        let mut sorted_vector = Vec::new();
        // Fill the vector with each pixel (node element)
        for i in 0..img.dimensions().0 {
            for j in 0..img.dimensions().1 {
                // Get the pixel's value. The unwrap is safe: we just run through the image
                // dimensions and there is no reason for the code to panic
                let val = img.get_pixel(i, j).0.first().unwrap().to_owned();
                let pixel = Pixel::new(i, j, val);
                sorted_vector.push(pixel);
            }
        }
        // Sort the vector with the standard function
        sorted_vector.sort_by(|a, b| a.value.cmp(&b.value));
        log::info!("MaxTree sort complete");
        log::debug!("MaxTree sort result: {:?}", sorted_vector);
        // A few sanity checks
        assert!(
            sorted_vector.len() == img.len(),
            "Expected the sorted vector's size to be {}, got {}",
            img.len(),
            sorted_vector.len()
        );
        sorted_vector
    }
}

/// Sorts the pixels of the image (interpolate image) from brightest to darkest
pub struct InterpolationSorter;

type Q = HashMap<u8, Vec<Pixel>>;


impl InterpolationSorter {

    fn push_q(q : &mut Q, l : &u8, pix : Pixel) {
        if q.get(l).is_none() {
            q.insert(*l, Vec::new());
        }

        let mut pix_ = pix;
        pix_.value = l.clone();

        q.get_mut(l)
            .unwrap()
            .push(pix_);
    }

    fn pop_q(q : &mut Q, l : &u8) -> Pixel {
        let val = q.get_mut(l)
                        .unwrap()
                        .pop().unwrap();

        if q.get_mut(l).unwrap().len() == 0 {
            q.remove(l);
        }

        return val;
    }


    fn priority_push(q : &mut Q, h : Pixel, img : &RgbImage, l : &u8) {
        let interval = img
            .get_pixel(h.x, h.y);
        let l_;

        if interval.0[0] > *l {
            l_ = interval.0[0];
        }
        else if interval.0[2] < *l{
            l_ = interval.0[2];
        }
        else {
            l_ = *l;
        }

        InterpolationSorter::push_q(q, &l_, h);
    }

    fn priority_pop(q : &mut Q, l : &mut u8) -> Pixel{

        let mut next : i16 = 1; 
        let mut level : i16 = *l as i16;

        while   q.get(&l).is_none() || 
                q.get(&l).unwrap().len() == 0 {
                if *l >= u8::MAX {
                    next = -1;
                }

                level += next;

                *l = level as u8;
        }
               
        return InterpolationSorter::pop_q(q, &l);
    }

    pub fn sort(img: &RgbImage) -> (Vec<Pixel>, GrayImage) {
        log::info!("Starting InterpolationSorter sort...");
       
        let mut r = Vec::new();
        let mut q = Q::new();
    
        let mut img2 = RgbImage::new(img.width() + 2, img.height() + 2);
        let mut already_seen = GrayImage::new(img2.width(), img2.height());
        let mut u_b = GrayImage::new(img.width(), img.height());
        
        // Search median value border
        let mut value = Vec::new();
        for j in 0..img.height() {
            if j == 0 || j == img.height() - 1 {
                for i in 0..img.width() {
                    value.push(img.get_pixel(i, j).0[1]);
                }
            }
            else {
                value.push(img.get_pixel(0, j).0[1]);
                value.push(img.get_pixel(img.width() - 1, j).0[1]);
            }
        }
        let l_inf = value[value.len() / 2];

        // Add border of the original image
        for j in 0..img2.height() {
            if j == 0 || j == img2.height() - 1 {
                for i in 0..img2.width() {
                    img2.put_pixel(i, j, Rgb([l_inf, l_inf, l_inf]));
                }
            }
            else {
                img2.put_pixel(0, j, Rgb([l_inf, l_inf, l_inf]));
                img2.put_pixel(img2.width() - 1, j, Rgb([l_inf, l_inf, l_inf]));

                // Pixel of img
                for i in 1..img2.width()-1 {
                    img2.put_pixel(i, j, Rgb(img.get_pixel(i-1, j-1).0));
                }
            }
        }

        // Initialize algo
        for j in 0..img2.height() {
            for i in 0..img2.width() {
                already_seen.put_pixel(i, j, Luma([0]));
            }
        }
        let p_inf = Pixel::new(0,0,l_inf);
        InterpolationSorter::push_q(&mut q, &l_inf, p_inf);
        already_seen.put_pixel(p_inf.x, p_inf.y, Luma([1]));
        let mut l = l_inf;
        
        // Loop
        while q.len() != 0 {
            let val_pop = InterpolationSorter::priority_pop(&mut q, &mut l);

            // We dont add the border
            if  val_pop.x > 0 && val_pop.y > 0 &&
                val_pop.x <= u_b.width() && val_pop.y <= u_b.height() {
                    let mut pixel_add = val_pop.clone();
                    pixel_add.x -= 1;
                    pixel_add.y -= 1;
                    u_b.put_pixel(pixel_add.x, pixel_add.y, Luma([l]));
                    r.push(pixel_add);
                }


            for n in val_pop.neighbours() {
                if  n.x < img2.width() && 
                    n.y < img2.height() {
                    if  already_seen.get_pixel(n.x, n.y).0[0] == 0 {
                        let mut n2 = n.clone();
                        n2.value = img2.get_pixel(n.x, n.y).0[1];
                        InterpolationSorter::priority_push(&mut q,n2, &img2, &l);
                        already_seen.put_pixel(n.x, n.y, Luma([1]));
                    } 
                }
            }

        }


        u_b.save("out_u_b.png")
        .expect("Failed to write the image interpolate");


        log::info!("...Ending InterpolationSorter sort !");
        return (r, u_b);
    }
}