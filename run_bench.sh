#!/bin/sh

hyperfine -P size 100 1000 -D 100 'RUST_LOG=error target/release/projet-tigd -i\
data/bench{size}x{size}.bmp --use-original-colors -m shapes' --export-csv \
bench.csv -n {size}

./plot_bench.py