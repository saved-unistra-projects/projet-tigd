# Projet TIGD

Aricle link: [*A quasi-linear algorithm to compute the tree of shapes of nD image*](https://moodle.unistra.fr/pluginfile.php/442449/mod_resource/content/1/geraud.2013.ismm-1.pdf)

## Authors
**Raphael COLIN**
**Geoffroy HEURTEL**

## Requirements

### Mandatory requirements

The main requirement is `cargo`. It will manage the library dependencies on its own, and compile and run the program. [Cargo installation](https://doc.rust-lang.org/cargo/getting-started/installation.html)

### Optional requirements

The optionnal requirements are to visualize the output tree and run the benchmarks.

- For the trees you will need `dot`

- For the benchmarks, you will need `hyperfine` and `python3` with `matplotlib`, `sklearn` and `numpy`.

## Building and running the program

In order to build the program, simply run `cargo build`, or `cargo build --release` for the optimized build.

In order to run the debug build of the program, you can use `cargo run`, if you want the 
release build, add the `--release` option to the command.

Be careful, options passed to the actual program (and not `cargo run`) need to be passed after the `--` separator like this: `cargo run -- [OPTIONS]`

### Options

You can get a list of available options by using the `--help` flag.

Note that the `--input` or `-i` option is mandatory.

## Visualizing the output

Once the program is done running, you will get 2 files: a `.dot` file and a `.png` file (by default they are named `out.dot` and `out_ref.png`).

The `.dot` file represents the computed tree, and can be turned into a PNG image by running the `dot_to_png.sh` script. Simply pass the name of the
`.dot` file (**without** the `.dot` extension) as an argument. It will then generate a file of the same name with the `.png` extension.

The `.png` file is a reference file. Since by default the nodes in the tree all have different hues, the representative pixels of each node
have been colored with the same hue in the image. This (partly) colored image is the `.png` file (`out_ref.png` by default)

## Running the benchmarks

If you have `hyperfine` installed as well as `python3` with its requirements, you can run the benchmarks
by using the `run_bench.sh` script. The benchmark data is made of the `bench*.bmp` files in the `data` directory.

This will run `hyperfine` (an easy to use benchmarking tool) on the different images with increasing sizes to
compute the mean execution time of each one, and then display the corresponding graph.

## Building documentation

You can use `cargo` to build and open the project documentation with `cargo doc --no-deps --open`.

## Results

Here are some images showing the results of the program on different examples as well as a benchmark result.

### Computing the max tree

Reference image | Computed max tree
----------------|------------------
![Reference image](figures/example_max.png) | ![Computed max tree](figures/example_max_res.png)
![Reference image](figures/test3.png) | ![Computed max tree](figures/test3_max_res.png)

### Computing the tree of shapes

Reference image | Computed tree of shapes
----------------|------------------
![Reference image](figures/example.png) | ![Computed tree of shapes](figures/example_res.png)
![Reference image](figures/test3.png) | ![Computed tree of shapes](figures/test3_res.png)

### Benchmark results for the tree of shapes computation

|Benchmark plot showing the linear complexity of the algorithm and its implementation|
|-------------------------------------------------------------------------------------|
|![](figures/plot.png)|